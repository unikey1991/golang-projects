package utils

import (
	"regexp"

	"github.com/boltdb/bolt"
	"time"
	"log"
)

// утилитный метод проверки телефона на корректность
func ValidatePhone(phone string) (isValid bool) {
	var validPhone = regexp.MustCompile("^[+][3][8][0][0-9\\s\\(\\)\\+]{9}$")
	if !validPhone.MatchString(phone) {
		return false
	}
	return true
}

func GetTransaction(writable bool) (*bolt.Tx, error)  {
	db, err := bolt.Open("my.db", 0600, &bolt.Options{Timeout: 1 * time.Second})
	if err != nil {
		log.Fatal("begin error", err)
		return nil, err
	}
	tx, err := db.Begin(writable)
	if err != nil {
		log.Fatal("begin error", err)
		return nil, err
	}

	return tx, nil
}

/*func GetBook(fname string) (*models.PhoneBook)  {
	in, err := ioutil.ReadFile(fname)
	if err != nil {
		if os.IsNotExist(err) {
			fmt.Printf("%s: Файл пока еще не создан. Создание нового файла...\n", fname)
			os.Create(fname)
		} else {
			log.Fatalln("Error reading file:", err)
		}
	}
	// [START marshal_proto]
	book := &models.PhoneBook{}
	// [START_EXCLUDE]
	if err := proto.Unmarshal(in, book); err != nil {
		log.Fatalln("Failed to parse address book:", err)
	}
	return book
}*/
