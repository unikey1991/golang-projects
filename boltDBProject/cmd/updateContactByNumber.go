// Copyright © 2017 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"boltDBProject/utils"
	"github.com/golang/protobuf/proto"
	"boltDBProject/models"
	"log"
)

// updateContactByNumberCmd represents the updateContactByNumber command
var updateContactByNumberCmd = &cobra.Command{
	Use:   "updateContactByNumber",
	Short: "A brief description of your command",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {
		//проверка валидности телефона и количество аргументов
		if len(args) != 6 || !utils.ValidatePhone(args[0]) {
			fmt.Println("Неверное использование команды")
			fmt.Println("addNewContact [телефон в формате +380844584870] [имя] " +
				"[фамилия] [город] [улица] [дом]")
			return
		}

		// Start the transaction.
		tx, _ := utils.GetTransaction(true)
		defer tx.Rollback()
		bkt := tx.Bucket([]byte(BucketName))
		if bkt == nil {
			log.Fatal("Bucket not exist, create first ")
			return
		}
		if row := bkt.Get([]byte(args[0])); row != nil {
			note := &models.Note{}
			if err := proto.Unmarshal(row, note); err != nil {
				log.Fatalln("Failed to parse note:", err)
			}
			note.Name = args[1]
			note.LastName = args[2]
			note.HomeAddress.City = args[3]
			note.HomeAddress.Streat = args[4]
			note.HomeAddress.HouseNumber = args[5]

			if buf, err := proto.Marshal(note); err != nil {
				log.Fatal(err)
				return
			} else if err := bkt.Put([]byte(args[0]), buf); err != nil {
				log.Fatal(err)

				return
			}

			if err := tx.Commit(); err != nil {
				log.Fatal(err)
				return
			}

			fmt.Println("Запись '" + args[0] + "' успешно обновлена")
		} else {
			fmt.Println("Номер " + args[0] + " не найден в базе")
		}
	},
}

func init() {
	rootCmd.AddCommand(updateContactByNumberCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// updateContactByNumberCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// updateContactByNumberCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
