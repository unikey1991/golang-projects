// Copyright © 2017 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"github.com/boltdb/bolt"
	"time"
	"log"
	"github.com/golang/protobuf/proto"
	"boltDBProject/models"
)

// getAllPhonebookCmd represents the getAllPhonebook command
var getAllPhonebookCmd = &cobra.Command{
	Use:   "getAllPhonebook",
	Short: "A brief description of your command",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {
		db, err := bolt.Open("my.db", 0600, &bolt.Options{Timeout: 1 * time.Second})
		if err != nil {
			log.Fatal("Open db error", err)
		}
		tx, err := db.Begin(false)
		if err != nil {
			log.Fatal("begin error", err)
			return
		}

		//err = db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(BucketName))
		if b == nil {
			log.Fatal("Bucket not exist, create first ")
			return
		}

		b.ForEach(func(k, v []byte) error {
			note := &models.Note{}
			if err := proto.Unmarshal(v, note); err != nil {
				log.Fatalln("Failed to parse note:", err)
			}
			fmt.Printf("key=%s, value=%s\n", k, note)
			return nil
		})
		//return nil
		//})

	},
}

func init() {
	rootCmd.AddCommand(getAllPhonebookCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// getAllPhonebookCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// getAllPhonebookCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
