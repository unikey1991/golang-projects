Installing:
===
###### 1. Скачать данный проект <https://gitlab.com/unikey1991/golang-projects/repository/master/archive.zip> 
 и из архива скопировать protobufProject в папку проектов Go

###### 2. Установить зависимоти Cobra, protobuf, boltDB и в настройках проекта (при необходимости) установить корректный goPath:

    go get -u github.com/spf13/cobra/cobra
    go get github.com/golang/protobuf/protoc-gen-go
    go get github.com/boltdb/bolt/...

###### 3. Установка компилятора protobuf (при его отсутствии):
    
* перейдите по ссылке <https://github.com/google/protobuf/releases>
* скачайте zip архив protoc-3.5.0-win32.zip
* распакуйте и положите эту папку в $GOPATH/bin

* консоле перейдите в $GOPATH/bin/protoc-3.5.0-win32/bin
* из этой директории запустите команду: `protoc -I=$GOPATH\src\protobufProject --go_out=$GOPATH\src\protobufProject $GOPATH\src\[имя проекта]\model\phoneBook.proto`
* (либо из терминала `protoc -I=models --go_out=models models\phoneBook.proto`)
* после выполнения у вас в директории $GOPATH\src\protobufProject\model сгенерится структура phoneBook.pb.go

Runing:
===

###### После установки в терминале перейти в директорию проект, ввести go run main.go и следовать подсказкам терминала

Available Commands:
===
                                         |                                             |
-----------------------------------------|:-------------------------------------------:|
  `go run main.go addNewContact`         |  **Добавление новой записи**                |
  `go run main.go deleteContactByNumber` |  **Удаление записи по номеру телефона**     |
  `go run main.go getAllPhonebook`       |  **Вывод всех записей справочника**         |
  `go run main.go help`                  |  **Help about any command**                 |
  `go run main.go searchContactByNumber` |  **Поиск записи по указанному номеру**      |
  `go run main.go updateContactByNumber` |  **Обновление данных по номеру телефона**   |

Usage example: 
===
    
    go run main.go addNewContact +380845615870 John Cena Dnepr Glinki 2




Ссылка на документацию по boltDB
===
<https://github.com/boltdb/bolt#installing>