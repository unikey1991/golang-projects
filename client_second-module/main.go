package main

import (
	"time"
	"os"
	"fmt"
	"encoding/csv"
	"google.golang.org/grpc"
	"log"
	"context"
	model "client_second-module/models"
	"io"
)

const (
	csvName         = "D:\\test.csv"
	BucketName      = "NOTES"
	address         = "10.1.205.24:8082"
	deadlineMinutes = 60

)

var i int64 = 0


func main() {
	start := time.Now()

	sendAllNotesToBoltDb()

	t := time.Now()
	elapsed := t.Sub(start)
	fmt.Println(elapsed)
}


func sendAllNotesToBoltDb() {
	conn, err := grpc.Dial(address, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()

	client := model.NewNoteServiceClient(conn)
	//ctx, _ := context.WithTimeout(context.Background(), deadlineMinutes* time.Minute)


	/////
	csvFile, err := os.Open(csvName)
	if err != nil {
		fmt.Println(err)
	}
	defer csvFile.Close()

	reader := csv.NewReader(csvFile)
	reader.FieldsPerRecord = -1


	//stream, err := client.LoadFromFileToBoltDb(ctx)
	stream, err := client.LoadFromFileToBoltDb(context.Background())
	if err != nil {
		log.Fatalf("%v.LoadFromFileToBoltDb(_) = _, %v", client, err)
	}

	for {
		csvData, err := reader.Read()
		if err == io.EOF {
			break
		}
		i++
		newNote := &model.Note{
			Phone:    csvData[0],
			Name:     csvData[1],
			LastName: csvData[2],
			HomeAddress: &model.Note_Address{
				City:        csvData[3],
				Street:      csvData[4],
				HouseNumber: csvData[5],
			}}
		if err := stream.Send(newNote); err != nil {
			log.Fatalf("%v.Send(%v) = %v", stream, newNote, err)
		}
	}
	reply, err := stream.CloseAndRecv()
	if err != nil {
		fmt.Println(err)
		log.Fatalf("%v.CloseAndRecv() got error %v, want %v", stream, err, nil)
	}
	log.Printf("Записей отправлено: %d", i)
	log.Printf("Статус: %s. Записей получено: %d. Записей записано в базу: %d", reply.Status, reply.NotesRecived, reply.NotesInBaseWrited)
}