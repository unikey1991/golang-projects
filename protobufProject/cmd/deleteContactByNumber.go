// Copyright © 2017 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"io/ioutil"
	"github.com/golang/protobuf/proto"
	"protobufProject/utils"
	"log"
)

const userFile string = "t.txt"

// deleteContactByNumberCmd represents the deleteContactByNumber command
var deleteContactByNumberCmd = &cobra.Command{
	Use:   "deleteContactByNumber [телефон в формате +380844584870]",
	Short: "Удаление записи по номеру телефона",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {
		//проверка корректности номера и количество аргументов
		if len(args) != 1 || !utils.ValidatePhone(args[0]) {
			fmt.Println("Неверное использование команды")
			fmt.Println("delete [телефон в формате +380844584870]")
			return
		}
		filename := "a.txt"

		book := utils.GetBook(filename)

		//поиск строки для перезаписи и перезапись если нашли
		for i, row := range book.NoteRow {
			if row.Phone == args[0]{
				lastIndex := len(book.NoteRow) - 1
				book.NoteRow[i], book.NoteRow[lastIndex] = book.NoteRow[lastIndex], book.NoteRow[i]
				book.NoteRow = book.NoteRow[:lastIndex]
				fmt.Println("Запись по номеру " + args[0] + " удалена успешно")
				out, err := proto.Marshal(book)
				if err != nil {
					log.Fatalln("Failed to encode address book:", err)
				}
				if err := ioutil.WriteFile(filename, out, 0644); err != nil {
					log.Fatalln("Failed to write address book:", err)
				}
				return
			}
		}
		fmt.Println("Номер " + args[0] + " не найден в базе")
	},
}

func init() {
	rootCmd.AddCommand(deleteContactByNumberCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// deleteContactByNumberCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// deleteContactByNumberCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
