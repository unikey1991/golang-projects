// Copyright © 2017 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"github.com/spf13/cobra"
	"protobufProject/models"
	"github.com/golang/protobuf/proto"
	"io/ioutil"
	"log"
	"protobufProject/utils"
	"fmt"
)

// addNewContactCmd represents the addNewContact command
var addNewContactCmd = &cobra.Command{
	Use:   "addNewContact [телефон в формате +380844584870] [имя] [фамилия] [адрес]",
	Short: "Добавление новой записи",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {

		//проверка валидности телефона и количество аргументов
		if len(args) != 6 || !utils.ValidatePhone(args[0]) {
			fmt.Println("Неверное использование команды")
			fmt.Println("addNewContact [телефон в формате +380844584870] [имя] " +
				"[фамилия] [город] [улица] [дом]")
			return
		}

		filename := "a.txt"
		book := utils.GetBook(filename)

		////создание модели из вводимых данных
		newNote := &models.Note{
			Phone:    args[0],
			Name:     args[1],
			LastName: args[2],
			HomeAddress: &models.Note_Address{
					City:        args[3],
					Streat:      args[4],
					HouseNumber: args[5],

			}}

		// поиск на существование записей в файле с таким номером
		for _, row := range book.NoteRow {
			if row.Phone == newNote.Phone {
				fmt.Println("Запись с таким номером уже существует")
				return
			}
		}
		book.NoteRow = append(book.NoteRow, newNote)
		// Запись в файл
		out, err := proto.Marshal(book)
		if err != nil {
			log.Fatalln("Failed to encode address book:", err)
		}
		if err := ioutil.WriteFile(filename, out, 0644); err != nil {
			log.Fatalln("Failed to write address book:", err)
		}
		fmt.Println("Запись '" + newNote.Phone + "' успешно создана")
	},
}

func init() {
	rootCmd.AddCommand(addNewContactCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// addNewContactCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// addNewContactCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
