// Copyright © 2017 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"fmt"

	"github.com/spf13/cobra"

	"protobufProject/utils"
)

// searchContactByNumberCmd represents the searchContactByNumber command
var searchContactByNumberCmd = &cobra.Command{
	Use:   "searchContactByNumber [телефон в формате +380844584870]",
	Short: "Поиск записи по указанному номеру",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {
		//проверка корректности номера
		if len(args) < 1 || !utils.ValidatePhone(args[0]) {
			fmt.Println("Номер телефона должен быть в формате +380934787780")
			return
		}

		filename := "a.txt"
		book := utils.GetBook(filename)

		// поиск на существование записей в файле с таким номером
		for _, row := range book.NoteRow {
			if row.Phone == args[0]{
				fmt.Println(row)
				return
			}
		}
		//если запись не найдена, то прерывания метода не будет и дойдет выполнения до выброса следующей строки
		fmt.Println("Номер " + args[0] + " не найден в базе")

	},
}

func init() {
	rootCmd.AddCommand(searchContactByNumberCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// searchContactByNumberCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// searchContactByNumberCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
