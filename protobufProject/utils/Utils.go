package utils

import (
	"regexp"
	"io/ioutil"
	"os"
	"fmt"
	"log"
	"github.com/golang/protobuf/proto"
	"protobufProject/models"
)

// утилитный метод проверки телефона на корректность
func ValidatePhone(phone string) (isValid bool) {
	var validPhone = regexp.MustCompile("^[+][3][8][0][0-9\\s\\(\\)\\+]{9}$")
	if !validPhone.MatchString(phone) {
		return false
	}
	return true
}

func GetBook(fname string) (*models.PhoneBook)  {
	in, err := ioutil.ReadFile(fname)
	if err != nil {
		if os.IsNotExist(err) {
			fmt.Printf("%s: Файл пока еще не создан. Создание нового файла...\n", fname)
			os.Create(fname)
		} else {
			log.Fatalln("Error reading file:", err)
		}
	}
	// [START marshal_proto]
	book := &models.PhoneBook{}
	// [START_EXCLUDE]
	if err := proto.Unmarshal(in, book); err != nil {
		log.Fatalln("Failed to parse address book:", err)
	}
	return book
}
