// Copyright © 2017 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"io/ioutil"
	"strings"
	"cobraProject/models"
	"cobraProject/utils"
)

// updateContactByNumberCmd represents the updateContactByNumber command
var updateContactByNumberCmd = &cobra.Command{
	Use:   "updateContactByNumber [новое имя] [новая фамилия] [новый адрес]",
	Short: "Обновление данных по номеру телефона",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {
		//проверка корректности номера и количество аргументов
		if len(args) != 4 || !utils.ValidatePhone(args[0]) {
			fmt.Println("Неверное использование команды")
			fmt.Println("update [телефон в формате +380844584870] [новое имя] [новая фамилия] [новый адрес]")
			return
		}
		/*создание модели из введенных данных*/
		note := models.Note{
			Phone: args[0],
			Name: args[1],
			LastName: args[2],
			Address: args[3],
		}
		//поиск строки для перезаписи и перезапись если нашли
		in, _ := ioutil.ReadFile(userFile)
		lines := strings.Split(string(in), "\n")
		for i, line := range lines {
			tmp := line
			lines[i] = models.GetNoteAsLine(note)
			out := strings.Join(lines, "\n")
			ioutil.WriteFile(userFile, []byte(out), 0644)
			fmt.Println("Запись " + tmp + " обновлена")
			fmt.Println("Новые данные:  " + lines[i])
			return
		}
		fmt.Println("Номер " + note.Phone + " не найден в базе")
	},
}

func init() {
	rootCmd.AddCommand(updateContactByNumberCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// updateContactByNumberCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// updateContactByNumberCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
