// Copyright © 2017 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"os"
	"io/ioutil"
	"strings"
	"cobraProject/models"
	"cobraProject/utils"
)

// addNewContactCmd represents the addNewContact command

var addNewContactCmd = &cobra.Command{
	Use:   "addNewContact [телефон в формате +380844584870] [имя] [фамилия] [адрес]",
	Short: "Добавление новой записи",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {
		//проверка валидности телефона и количество аргументов
		if len(args) != 4 || !utils.ValidatePhone(args[0]) {
			fmt.Println("Неверное использование команды")
			fmt.Println("addNewContact [телефон в формате +380844584870] [имя] [фамилия] [адрес]")
			return
		}
		//создание модели из вводимых данных
		note := models.Note{
			Phone: args[0],
			Name: args[1],
			LastName: args[2],
			Address: args[3],
		}


		file, err := os.OpenFile("t.txt", os.O_APPEND|os.O_WRONLY, 0600)
		defer file.Close()
		if err != nil {
			fmt.Println("Файл t.txt не найден")
			fmt.Println("Используйте create для создания файла")
			return
		}
		in, _ := ioutil.ReadFile("t.txt")
		lines := strings.Split(string(in), "\n")
		// поиск на существование записей в файле с таким номером
		for _, line := range lines {
			if strings.Split(line, " ")[0] == note.Phone {
				fmt.Println("Запись с таким номером уже существует")
				return
			}
		}
		//запись новой записи в файл
		if _, err = file.WriteString(models.GetNoteAsLine(note)); err != nil {
			fmt.Println("Запись в файл не удалась.")
			return
		}
		fmt.Println("Запись '" + models.GetNoteAsLine(note) + "' успешно создана")
	},
}

func init() {
	rootCmd.AddCommand(addNewContactCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// addNewContactCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// addNewContactCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}

