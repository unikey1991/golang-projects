package utils

import (
	"regexp"
)

// утилитный метод проверки телефона на корректность
func ValidatePhone(phone string) (isValid bool) {
	var validPhone = regexp.MustCompile("^[+][3][8][0][0-9\\s\\(\\)\\+]{9}$")
	if !validPhone.MatchString(phone) {
		return false
	}
	return true
}