package models

//поля модели записи
type Note struct {
	Phone string
	Name string
	LastName string
	Address string
}

// метод конвертации модели в строку
func GetNoteAsLine(note Note) string {
	return note.Phone + " " + note.Name + " " + note.LastName + " " + note.Address
}