package main

import (
	"os"
	"fmt"
	model "FromDBToBoltGRPC/models"
	"encoding/csv"
	"encoding/json"
	"google.golang.org/grpc"
	"time"
	"log"
	"context"
)

const (
	dbConnString = "root:qwerty@tcp(localhost:3306)/testgrpc"
	csvName      = "test.csv"
	jsonName     = "test.json"
	protobufName = "test.protobuf"
	pathToDb     = "my.db"
	BucketName   = "NOTES"
	ADDRESS          = "10.1.205.24:8082"
	DEADLINE_MINUTES = 10

)



func main() {
	//convertCsvToJson() // конвертация csv to json

	sendAllNotesToBoltDb()

}
func sendAllNotesToBoltDb() {
	conn, err := grpc.Dial(ADDRESS, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}

	defer conn.Close()

	client := model.NewNoteServiceClient(conn)
	ctx, _ := context.WithTimeout(context.Background(), DEADLINE_MINUTES* time.Minute)


	/////
	csvFile, err := os.Open(csvName)
	if err != nil {
		fmt.Println(err)
	}
	defer csvFile.Close()

	reader := csv.NewReader(csvFile)
	reader.FieldsPerRecord = -1

	csvData, err := reader.ReadAll()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	stream, err := client.LoadFromFileToBoltDb(ctx)
	if err != nil {
		log.Fatalf("%v.LoadFromFileToBoltDb(_) = _, %v", client, err)
	}

	for _, each := range csvData {

		newNote := &model.Note{
			Phone:    each[0],
			Name:     each[1],
			LastName: each[2],
			HomeAddress: &model.Note_Address{
				City:        each[3],
				Street:      each[4],
				HouseNumber: each[5],
			}}
		if err := stream.Send(newNote); err != nil {
			log.Fatalf("%v.Send(%v) = %v", stream, newNote, err)
		}
	}

	reply, err := stream.CloseAndRecv()
	if err != nil {
		log.Fatalf("%v.CloseAndRecv() got error %v, want %v", stream, err, nil)
	}
	log.Printf("Notes summary: %v", reply)
}





func convertCsvToJson() {
	csvFile, err := os.Open(csvName)
	if err != nil {
		fmt.Println(err)
	}
	defer csvFile.Close()

	reader := csv.NewReader(csvFile)
	reader.FieldsPerRecord = -1

	csvData, err := reader.ReadAll()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	var note model.Note
	var notes []model.Note

	for _, each := range csvData {
		note.Phone = each[0]
		note.Name = each[1]
		note.LastName = each[2]
		note.HomeAddress = &model.Note_Address{City: each[3], Street: each[4], HouseNumber: each[5]}
		notes = append(notes, note)
	}

	// Convert to JSON
	jsonData, err := json.Marshal(notes)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	fmt.Println(string(jsonData))

	jsonFile, err := os.Create(jsonName)
	if err != nil {
		fmt.Println(err)
	}
	defer jsonFile.Close()

	jsonFile.Write(jsonData)
	jsonFile.Close()
}
