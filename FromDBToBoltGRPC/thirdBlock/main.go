package main

import (
	"github.com/golang/protobuf/proto"
	"github.com/boltdb/bolt"
	model "FromDBToBoltGRPC/models"
	"fmt"
	"time"
	"io/ioutil"
	"os"
	"errors"
	"log"
	"net"
	"google.golang.org/grpc"
	"io"
)

const (
	protobufName = "test.protobuf"
	pathToDb     = "my.db"
	BucketName   = "NOTES"
	grpcPort     = ":8082"
)

type Server struct{}

func (s *Server) LoadFromFileToBoltDb(stream model.NoteService_LoadFromFileToBoltDbServer) error {
	DB, _ := bolt.Open(pathToDb, 0600, &bolt.Options{Timeout: 1 * time.Second})

	fmt.Println("test LoadFromFileToBoltDb")

	err := DB.Update(func(tx *bolt.Tx) error {
		bkt, err := tx.CreateBucketIfNotExists([]byte(BucketName))
		if err != nil {
			return fmt.Errorf("create bucket: %s", err)
		}
		for {
			newNote, err := stream.Recv()
			if err == io.EOF {
				return nil
			}
			if err != nil {
				return err
			}
			if buf, err := proto.Marshal(newNote); err != nil {
				return errors.New("Marshaling error")
			} else if err := bkt.Put([]byte(newNote.Phone), buf); err != nil {
				fmt.Println(err)
				return errors.New("Writing in base error")
			}
		}
		return nil //You commit the transaction by returning nil at the end
	})
	if err != nil {
		panic(err)

	}
	DB.Close()
	return nil
}

func main() {

	startGrpc()
	writeToBoltDbFromProtobuf()

	printAllFromBoltDb()
}

func startGrpc() {
	// start listening for grpc
	listen, err := net.Listen("tcp", grpcPort)
	if err != nil {
		log.Fatal(err)
	}
	// Instanciate new gRPC server
	server := grpc.NewServer()
	//register service
	model.RegisterNoteServiceServer(server, new(Server))

	log.Println("Starting grpc server on port " + grpcPort)

	// Start the gRPC server in goroutine
	//go server.Serve(listen)
	server.Serve(listen)

}

func printAllFromBoltDb() {
	DB, _ := bolt.Open(pathToDb, 0600, &bolt.Options{Timeout: 1 * time.Second})

	fmt.Println("printAllFromBoltDb:")
	err := DB.Update(func(tx *bolt.Tx) error {
		bkt := tx.Bucket([]byte(BucketName))
		if bkt == nil {
			return errors.New("Bucket not exist, create first")
		}
		bkt.ForEach(func(k, v []byte) error {
			note := &model.Note{}
			if err := proto.Unmarshal(v, note); err != nil {
				return err
			}
			fmt.Println(note)
			return nil
		})
		return nil
	})
	if err != nil {
		panic(err)
	}

	DB.Close()
}
func writeToBoltDbFromProtobuf() {
	DB, _ := bolt.Open(pathToDb, 0600, &bolt.Options{Timeout: 1 * time.Second})

	in, err := ioutil.ReadFile(protobufName)
	if err != nil {
		if os.IsNotExist(err) {
			fmt.Printf("%s: Файл пока еще не создан. Создание нового файла...\n", protobufName)
			os.Create(protobufName)
		} else {
			log.Fatalln("Error reading file:", err)
		}
	}

	notesList := &model.ListResponse{}

	if err := proto.Unmarshal(in, notesList); err != nil {
		log.Fatalln("Failed to parse address book:", err)
	}

	err = DB.Update(func(tx *bolt.Tx) error {
		for _, note := range notesList.Note {
			bkt, err := tx.CreateBucketIfNotExists([]byte(BucketName))
			if err != nil {
				return fmt.Errorf("create bucket: %s", err)
			}

			if buf, err := proto.Marshal(note); err != nil {
				return errors.New("Marshaling error")
			} else if err := bkt.Put([]byte(note.Phone), buf); err != nil {
				fmt.Println(err)
				return errors.New("Writing in base error")
			}
		}
		return nil //You commit the transaction by returning nil at the end
	})
	if err != nil {
		panic(err)

	}
	DB.Close()

}

func writeToBoltDbFromJson() {
	//DB, _ = bolt.Open(pathToDb, 0600, &bolt.Options{Timeout: 1 * time.Second})
	//
	//err := DB.Update(func(tx *bolt.Tx) error {
	//bkt, err := tx.CreateBucketIfNotExists([]byte(BucketName))
	//if err != nil {
	//	return fmt.Errorf("create bucket: %s", err)
	//}
	//
	//if row := bkt.Get([]byte(note.Phone)); row != nil {
	//	fmt.Println("trest")
	//	return errors.New("Запись с таким номером уже существует")
	//}
	//if buf, err := proto.Marshal(note); err != nil {
	//	return errors.New("Marshaling error")
	//} else if err := bkt.Put([]byte(note.Phone), buf); err != nil {
	//	fmt.Println(err)
	//	return errors.New("Writing in base error")
	//}
	//	return nil //You commit the transaction by returning nil at the end
	//})
	//if err != nil {
	//	panic(err)
	//}
	//panic(err)
}
