package main

import (
	"github.com/boltdb/bolt"
	"time"
	"fmt"

)
const (
	pathToDb     = "my.db"
	BucketName   = "NOTES"
)
func main()  {

	DB, _ := bolt.Open(pathToDb, 0600, &bolt.Options{Timeout: 1 * time.Second})
	err := DB.Update(func(tx *bolt.Tx) error {
		bkt, err := tx.CreateBucketIfNotExists([]byte(BucketName))
		if err != nil {
			return fmt.Errorf("create bucket: %s", err)
		}

		stats := bkt.Stats()

		fmt.Println(stats.Depth)
		fmt.Println(stats.LeafInuse)
		fmt.Println(stats.KeyN)
		fmt.Println(stats.LeafPageN)

		return nil
	})
	if err != nil {
		panic(err)

	}
	DB.Close()
}


