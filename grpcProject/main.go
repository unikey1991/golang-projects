package grpcProject

import (
	"flag"
	"github.com/golang/glog"
	"net/http"

	"golang.org/x/net/context"
	"github.com/grpc-ecosystem/grpc-gateway/runtime"
)






func run() error {
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	mux := runtime.NewServeMux()

	http.ListenAndServe(":8080", mux)
	return nil
}

func main() {
	flag.Parse()
	defer glog.Flush()

	if err := run(); err != nil {
		glog.Fatal(err)
	}
}