// Code generated by protoc-gen-go. DO NOT EDIT.
// source: models/phoneBook.proto

/*
Package grpcProject_models is a generated protocol buffer package.

It is generated from these files:
	models/phoneBook.proto

It has these top-level messages:
	RequestByPhoneNumber
	ListResponse
	Request
	Response
*/
package grpcProject_models

import proto "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"
import grpcProject_entity "grpcProject/entity"
import _ "google.golang.org/genproto/googleapis/api/annotations"
import _ "github.com/grpc-ecosystem/grpc-gateway/protoc-gen-swagger/options"

import (
	context "golang.org/x/net/context"
	grpc "google.golang.org/grpc"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion2 // please upgrade the proto package

type Response_RespStatus int32

const (
	Response_OK           Response_RespStatus = 0
	Response_NOT_FOUND    Response_RespStatus = 1
	Response_BAD_REQUEST  Response_RespStatus = 2
	Response_CREATED      Response_RespStatus = 3
	Response_SERVER_ERROR Response_RespStatus = 4
	Response_UPDATED      Response_RespStatus = 5
)

var Response_RespStatus_name = map[int32]string{
	0: "OK",
	1: "NOT_FOUND",
	2: "BAD_REQUEST",
	3: "CREATED",
	4: "SERVER_ERROR",
	5: "UPDATED",
}
var Response_RespStatus_value = map[string]int32{
	"OK":           0,
	"NOT_FOUND":    1,
	"BAD_REQUEST":  2,
	"CREATED":      3,
	"SERVER_ERROR": 4,
	"UPDATED":      5,
}

func (x Response_RespStatus) String() string {
	return proto.EnumName(Response_RespStatus_name, int32(x))
}
func (Response_RespStatus) EnumDescriptor() ([]byte, []int) { return fileDescriptor0, []int{3, 0} }

type RequestByPhoneNumber struct {
	PhoneNumber string `protobuf:"bytes,1,opt,name=phoneNumber" json:"phoneNumber,omitempty"`
}

func (m *RequestByPhoneNumber) Reset()                    { *m = RequestByPhoneNumber{} }
func (m *RequestByPhoneNumber) String() string            { return proto.CompactTextString(m) }
func (*RequestByPhoneNumber) ProtoMessage()               {}
func (*RequestByPhoneNumber) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{0} }

func (m *RequestByPhoneNumber) GetPhoneNumber() string {
	if m != nil {
		return m.PhoneNumber
	}
	return ""
}

type ListResponse struct {
	Note []*grpcProject_entity.Note `protobuf:"bytes,1,rep,name=note" json:"note,omitempty"`
}

func (m *ListResponse) Reset()                    { *m = ListResponse{} }
func (m *ListResponse) String() string            { return proto.CompactTextString(m) }
func (*ListResponse) ProtoMessage()               {}
func (*ListResponse) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{1} }

func (m *ListResponse) GetNote() []*grpcProject_entity.Note {
	if m != nil {
		return m.Note
	}
	return nil
}

type Request struct {
	Note *grpcProject_entity.Note `protobuf:"bytes,1,opt,name=note" json:"note,omitempty"`
}

func (m *Request) Reset()                    { *m = Request{} }
func (m *Request) String() string            { return proto.CompactTextString(m) }
func (*Request) ProtoMessage()               {}
func (*Request) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{2} }

func (m *Request) GetNote() *grpcProject_entity.Note {
	if m != nil {
		return m.Note
	}
	return nil
}

type Response struct {
	Note   *grpcProject_entity.Note `protobuf:"bytes,1,opt,name=note" json:"note,omitempty"`
	Status Response_RespStatus      `protobuf:"varint,2,opt,name=status,enum=grpcProject.models.Response_RespStatus" json:"status,omitempty"`
}

func (m *Response) Reset()                    { *m = Response{} }
func (m *Response) String() string            { return proto.CompactTextString(m) }
func (*Response) ProtoMessage()               {}
func (*Response) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{3} }

func (m *Response) GetNote() *grpcProject_entity.Note {
	if m != nil {
		return m.Note
	}
	return nil
}

func (m *Response) GetStatus() Response_RespStatus {
	if m != nil {
		return m.Status
	}
	return Response_OK
}

func init() {
	proto.RegisterType((*RequestByPhoneNumber)(nil), "grpcProject.models.RequestByPhoneNumber")
	proto.RegisterType((*ListResponse)(nil), "grpcProject.models.ListResponse")
	proto.RegisterType((*Request)(nil), "grpcProject.models.Request")
	proto.RegisterType((*Response)(nil), "grpcProject.models.Response")
	proto.RegisterEnum("grpcProject.models.Response_RespStatus", Response_RespStatus_name, Response_RespStatus_value)
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConn

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion4

// Client API for NoteService service

type NoteServiceClient interface {
	// Get all Customers with filter - A server-to-client streaming RPC.
	GetAllNotes(ctx context.Context, in *Request, opts ...grpc.CallOption) (*ListResponse, error)
	// Create a new Customer - A simple RPC
	CreateNote(ctx context.Context, in *Request, opts ...grpc.CallOption) (*Response, error)
	DelNoteByNumber(ctx context.Context, in *RequestByPhoneNumber, opts ...grpc.CallOption) (*Response, error)
	SearchNoteByNum(ctx context.Context, in *RequestByPhoneNumber, opts ...grpc.CallOption) (*Response, error)
	UpdateNote(ctx context.Context, in *Request, opts ...grpc.CallOption) (*Response, error)
}

type noteServiceClient struct {
	cc *grpc.ClientConn
}

func NewNoteServiceClient(cc *grpc.ClientConn) NoteServiceClient {
	return &noteServiceClient{cc}
}

func (c *noteServiceClient) GetAllNotes(ctx context.Context, in *Request, opts ...grpc.CallOption) (*ListResponse, error) {
	out := new(ListResponse)
	err := grpc.Invoke(ctx, "/grpcProject.models.NoteService/GetAllNotes", in, out, c.cc, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *noteServiceClient) CreateNote(ctx context.Context, in *Request, opts ...grpc.CallOption) (*Response, error) {
	out := new(Response)
	err := grpc.Invoke(ctx, "/grpcProject.models.NoteService/CreateNote", in, out, c.cc, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *noteServiceClient) DelNoteByNumber(ctx context.Context, in *RequestByPhoneNumber, opts ...grpc.CallOption) (*Response, error) {
	out := new(Response)
	err := grpc.Invoke(ctx, "/grpcProject.models.NoteService/DelNoteByNumber", in, out, c.cc, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *noteServiceClient) SearchNoteByNum(ctx context.Context, in *RequestByPhoneNumber, opts ...grpc.CallOption) (*Response, error) {
	out := new(Response)
	err := grpc.Invoke(ctx, "/grpcProject.models.NoteService/SearchNoteByNum", in, out, c.cc, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *noteServiceClient) UpdateNote(ctx context.Context, in *Request, opts ...grpc.CallOption) (*Response, error) {
	out := new(Response)
	err := grpc.Invoke(ctx, "/grpcProject.models.NoteService/UpdateNote", in, out, c.cc, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// Server API for NoteService service

type NoteServiceServer interface {
	// Get all Customers with filter - A server-to-client streaming RPC.
	GetAllNotes(context.Context, *Request) (*ListResponse, error)
	// Create a new Customer - A simple RPC
	CreateNote(context.Context, *Request) (*Response, error)
	DelNoteByNumber(context.Context, *RequestByPhoneNumber) (*Response, error)
	SearchNoteByNum(context.Context, *RequestByPhoneNumber) (*Response, error)
	UpdateNote(context.Context, *Request) (*Response, error)
}

func RegisterNoteServiceServer(s *grpc.Server, srv NoteServiceServer) {
	s.RegisterService(&_NoteService_serviceDesc, srv)
}

func _NoteService_GetAllNotes_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(Request)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(NoteServiceServer).GetAllNotes(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/grpcProject.models.NoteService/GetAllNotes",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(NoteServiceServer).GetAllNotes(ctx, req.(*Request))
	}
	return interceptor(ctx, in, info, handler)
}

func _NoteService_CreateNote_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(Request)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(NoteServiceServer).CreateNote(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/grpcProject.models.NoteService/CreateNote",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(NoteServiceServer).CreateNote(ctx, req.(*Request))
	}
	return interceptor(ctx, in, info, handler)
}

func _NoteService_DelNoteByNumber_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(RequestByPhoneNumber)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(NoteServiceServer).DelNoteByNumber(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/grpcProject.models.NoteService/DelNoteByNumber",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(NoteServiceServer).DelNoteByNumber(ctx, req.(*RequestByPhoneNumber))
	}
	return interceptor(ctx, in, info, handler)
}

func _NoteService_SearchNoteByNum_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(RequestByPhoneNumber)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(NoteServiceServer).SearchNoteByNum(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/grpcProject.models.NoteService/SearchNoteByNum",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(NoteServiceServer).SearchNoteByNum(ctx, req.(*RequestByPhoneNumber))
	}
	return interceptor(ctx, in, info, handler)
}

func _NoteService_UpdateNote_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(Request)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(NoteServiceServer).UpdateNote(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/grpcProject.models.NoteService/UpdateNote",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(NoteServiceServer).UpdateNote(ctx, req.(*Request))
	}
	return interceptor(ctx, in, info, handler)
}

var _NoteService_serviceDesc = grpc.ServiceDesc{
	ServiceName: "grpcProject.models.NoteService",
	HandlerType: (*NoteServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "GetAllNotes",
			Handler:    _NoteService_GetAllNotes_Handler,
		},
		{
			MethodName: "CreateNote",
			Handler:    _NoteService_CreateNote_Handler,
		},
		{
			MethodName: "DelNoteByNumber",
			Handler:    _NoteService_DelNoteByNumber_Handler,
		},
		{
			MethodName: "SearchNoteByNum",
			Handler:    _NoteService_SearchNoteByNum_Handler,
		},
		{
			MethodName: "UpdateNote",
			Handler:    _NoteService_UpdateNote_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "models/phoneBook.proto",
}

func init() { proto.RegisterFile("models/phoneBook.proto", fileDescriptor0) }

var fileDescriptor0 = []byte{
	// 591 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xb4, 0x54, 0x41, 0x4f, 0x13, 0x41,
	0x14, 0x76, 0x4b, 0x05, 0x79, 0x05, 0x59, 0x46, 0x43, 0x6a, 0xe5, 0xb0, 0xae, 0x31, 0x56, 0xa4,
	0xbb, 0xb4, 0x9a, 0xd8, 0x34, 0x26, 0xa6, 0xa5, 0x2b, 0x07, 0x4d, 0x8b, 0x53, 0xea, 0x15, 0x97,
	0xe5, 0x65, 0xbb, 0xb8, 0xcc, 0x0c, 0x3b, 0x53, 0x0c, 0x31, 0x5c, 0x8c, 0xbf, 0x40, 0x7f, 0x9a,
	0x47, 0xaf, 0x1e, 0xfc, 0x19, 0x66, 0x87, 0x16, 0x96, 0xd0, 0x54, 0x0e, 0x7a, 0x69, 0x77, 0xbe,
	0xf7, 0xbe, 0xf7, 0x7d, 0xf3, 0xe6, 0xe5, 0xc1, 0xca, 0x21, 0xdf, 0xc7, 0x58, 0xba, 0x62, 0xc0,
	0x19, 0xb6, 0x38, 0xff, 0xe8, 0x88, 0x84, 0x2b, 0x4e, 0x48, 0x98, 0x88, 0x60, 0x3b, 0xe1, 0x07,
	0x18, 0x28, 0xe7, 0x2c, 0xa7, 0x54, 0xce, 0x60, 0x2e, 0x32, 0x15, 0xa9, 0x93, 0x0b, 0x9e, 0xa7,
	0xcf, 0x67, 0xec, 0xd2, 0x6a, 0xc8, 0x79, 0x18, 0xa3, 0xeb, 0x8b, 0xc8, 0xf5, 0x19, 0xe3, 0xca,
	0x57, 0x11, 0x67, 0x72, 0x14, 0x5d, 0xd7, 0x7f, 0x41, 0x25, 0x44, 0x56, 0x91, 0x9f, 0xfc, 0x30,
	0xc4, 0xc4, 0xe5, 0x42, 0x67, 0x5c, 0xcd, 0xb6, 0xeb, 0x70, 0x97, 0xe2, 0xd1, 0x10, 0xa5, 0x6a,
	0x9d, 0x6c, 0xa7, 0x6a, 0x9d, 0xe1, 0xe1, 0x1e, 0x26, 0xc4, 0x82, 0x82, 0xb8, 0x38, 0x16, 0x0d,
	0xcb, 0x28, 0xcf, 0xd3, 0x2c, 0x64, 0xbf, 0x84, 0x85, 0xb7, 0x91, 0x54, 0x14, 0xa5, 0xe0, 0x4c,
	0x22, 0x59, 0x87, 0x3c, 0xe3, 0x0a, 0x8b, 0x86, 0x35, 0x53, 0x2e, 0xd4, 0x8a, 0x4e, 0xf6, 0x8a,
	0x67, 0xd7, 0x71, 0x3a, 0x5c, 0x21, 0xd5, 0x59, 0xf6, 0x0b, 0x98, 0x1b, 0xe9, 0x66, 0x88, 0xc6,
	0x35, 0x88, 0x3f, 0x0d, 0xb8, 0x35, 0x41, 0xf3, 0x1a, 0x54, 0xf2, 0x0a, 0x66, 0xa5, 0xf2, 0xd5,
	0x50, 0x16, 0x73, 0x96, 0x51, 0xbe, 0x5d, 0x7b, 0xec, 0x5c, 0x7d, 0x06, 0x67, 0x5c, 0x5b, 0x7f,
	0xf4, 0x74, 0x3a, 0x1d, 0xd1, 0xec, 0x0f, 0x00, 0x17, 0x28, 0x99, 0x85, 0x5c, 0xf7, 0x8d, 0x79,
	0x83, 0x2c, 0xc2, 0x7c, 0xa7, 0xbb, 0xb3, 0xfb, 0xba, 0xdb, 0xef, 0xb4, 0x4d, 0x83, 0x2c, 0x41,
	0xa1, 0xd5, 0x6c, 0xef, 0x52, 0xef, 0x5d, 0xdf, 0xeb, 0xed, 0x98, 0x39, 0x52, 0x80, 0xb9, 0x4d,
	0xea, 0x35, 0x77, 0xbc, 0xb6, 0x39, 0x43, 0x4c, 0x58, 0xe8, 0x79, 0xf4, 0xbd, 0x47, 0x77, 0x3d,
	0x4a, 0xbb, 0xd4, 0xcc, 0xa7, 0xe1, 0xfe, 0x76, 0x5b, 0x87, 0x6f, 0xd6, 0x7e, 0xe7, 0xa1, 0x90,
	0x3a, 0xee, 0x61, 0x72, 0x1c, 0x05, 0x48, 0x42, 0x28, 0x6c, 0xa1, 0x6a, 0xc6, 0x71, 0x0a, 0x4a,
	0x72, 0x7f, 0xb2, 0x63, 0xdd, 0xc7, 0x92, 0x35, 0x29, 0x98, 0x7d, 0x22, 0xfb, 0xde, 0x97, 0x1f,
	0xbf, 0xbe, 0xe7, 0xee, 0x90, 0x65, 0x3d, 0x3a, 0xc7, 0x55, 0xd7, 0x8f, 0xe3, 0x0a, 0xd3, 0x95,
	0x15, 0xc0, 0x66, 0x82, 0xbe, 0xc2, 0x54, 0x68, 0xba, 0xce, 0xea, 0xb4, 0xb6, 0xd9, 0x4f, 0xb5,
	0xc6, 0x23, 0xdb, 0x1a, 0x6b, 0x04, 0xba, 0xac, 0x96, 0x71, 0x3f, 0xa7, 0xbf, 0x8e, 0x9e, 0xa2,
	0xd3, 0x86, 0xb1, 0x46, 0xbe, 0x1a, 0xb0, 0xd4, 0x46, 0x7d, 0xb9, 0xd6, 0xc9, 0x68, 0xf2, 0xca,
	0x53, 0xb4, 0x2f, 0xcd, 0xe8, 0x5f, 0x8c, 0x3c, 0xd1, 0x46, 0x1e, 0xda, 0x0f, 0xc6, 0x46, 0xf6,
	0x31, 0xc6, 0x73, 0x23, 0x99, 0x49, 0x3e, 0x25, 0xa7, 0xb0, 0xd4, 0x43, 0x3f, 0x09, 0x06, 0xe7,
	0x46, 0xfe, 0x99, 0x0b, 0x5b, 0xbb, 0x58, 0x25, 0xa5, 0xb1, 0x8b, 0x09, 0xf2, 0x0a, 0xa0, 0x2f,
	0xf6, 0xff, 0x47, 0xef, 0x87, 0xba, 0xec, 0xc4, 0xde, 0xb7, 0xd4, 0xb7, 0xe6, 0x11, 0xd9, 0x82,
	0xc5, 0xf3, 0x1d, 0x53, 0x11, 0xcf, 0xeb, 0x76, 0x15, 0x96, 0x2f, 0x01, 0x15, 0x5f, 0x44, 0x64,
	0x65, 0xa0, 0x94, 0x68, 0xb8, 0x6e, 0xcc, 0x03, 0x3f, 0x1e, 0x70, 0xa9, 0x1a, 0xf5, 0x8d, 0x7a,
	0xd5, 0x2d, 0xe5, 0x19, 0x67, 0x58, 0x9b, 0xa9, 0x3a, 0x1b, 0x6b, 0x39, 0x23, 0x57, 0x33, 0x7d,
	0x21, 0xe2, 0x28, 0xd0, 0xfb, 0xc5, 0x3d, 0x90, 0x9c, 0x35, 0xae, 0x20, 0x7b, 0xb3, 0x7a, 0xed,
	0x3c, 0xfb, 0x13, 0x00, 0x00, 0xff, 0xff, 0xd8, 0x65, 0x7f, 0x14, 0x1a, 0x05, 0x00, 0x00,
}
