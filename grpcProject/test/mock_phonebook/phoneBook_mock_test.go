package mock_phonebook

/*
import (
	"github.com/golang/protobuf/proto"
	"fmt"
	"testing"
	"github.com/golang/mock/gomock"
	pb "grpcProject/models"
	"golang.org/x/net/context"
	//phoneBookMock "grpcProject/server/mock_phonebook"
)

// Reference imports to suppress errors if they are not otherwise used.

// rpcMsg implements the gomock.Matcher interface
type rpcMsg struct {
	msg proto.Message
}

func (r *rpcMsg) Matches(msg interface{}) bool {
	m, ok := msg.(proto.Message)
	if !ok {
		return false
	}
	return proto.Equal(m, r.msg)
}

func (r *rpcMsg) String() string {
	return fmt.Sprintf("is %s", r.msg)
}

//SearchNoteByNum(ctx context.Context, in *NoteByNumRequest, opts ...grpc.CallOption) (*NoteResponse, error)

func TestStopServer(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	mockNoteClient := phoneBookMock.NewMockNoteClient(ctrl)
	req := &pb.StopServerRequest{}
	mockNoteClient.EXPECT().StopServer(
		gomock.Any(),
		&rpcMsg{msg:req},
	).Return(&pb.StopServerResponse{Mesage:"Start of graceful stop server",}, nil)
	testStopServer(t, mockNoteClient)
}

func testStopServer(t *testing.T, client pb.NoteClient) {
	r, err := client.StopServer(context.Background(), &pb.StopServerRequest{})
	if err != nil || r.Mesage != "Start of graceful stop server" {
		t.Errorf("mocking failed")
	}
	t.Log("Reply : ", r)
}

func TestCreateNote(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	mockNoteClient := phoneBookMock.NewMockNoteClient(ctrl)
	req := &pb.NoteRequest{
		Phone: "+380548531570",
		Name: "Test1",
		LastName: "Test2",
		HomeAddress: &pb.NoteRequest_Address{
			 City: "Test city",
			 Streat: "Test street",
			 HouseNumber: "Test num",
		 },
	}
	mockNoteClient.EXPECT().CreateNote(
		gomock.Any(),
		&rpcMsg{msg:req},
	).Return(&pb.NoteResponse{
		Phone: "+380548531570",
		Name: "Test1",
		LastName: "Test2",
		HomeAddress: &pb.NoteResponse_Address{
			City: "Test city",
			Streat: "Test street",
			HouseNumber: "Test num",
		},
	}, nil)

	testCreateNote(t, mockNoteClient)
}

func testCreateNote(t *testing.T, client pb.NoteClient) {
	r, err := client.CreateNote(context.Background(), &pb.NoteRequest{
		Phone: "+380548531570",
		Name: "Test1",
		LastName: "Test2",
		HomeAddress: &pb.NoteRequest_Address{
			City: "Test city",
			Streat: "Test street",
			HouseNumber: "Test num",
		},
	})
	if err != nil || r.Phone != "+380548531570" || r.Name != "Test1" || r.LastName != "Test2" ||
	 r.HomeAddress.City != "Test city" || r.HomeAddress.Streat != "Test street" || r.HomeAddress.HouseNumber != "Test num"	{
		t.Errorf("mocking failed")
	}
	t.Log("Reply : ", r)
}


func TestSearchNoteByNum(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	mockNoteClient := phoneBookMock.NewMockNoteClient(ctrl)
	req := &pb.NoteByNumRequest{
		Phone: "+380548531570",
	}
	mockNoteClient.EXPECT().SearchNoteByNum(
		gomock.Any(),
		&rpcMsg{msg:req},
	).Return(&pb.NoteResponse{
		Phone: "+380548531570",
		Name: "Mocked Interface",
	}, nil)

	testSearchNoteByNum(t, mockNoteClient)
}

func testSearchNoteByNum(t *testing.T, client pb.NoteClient) {
	r, err := client.SearchNoteByNum(context.Background(), &pb.NoteByNumRequest{Phone: "+380548531570"})
	if err != nil || r.Name != "Mocked Interface" {
		t.Errorf("mocking failed")
	}
	t.Log("Reply : ", r)
}*/
