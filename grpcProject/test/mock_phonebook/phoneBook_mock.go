package mock_phonebook

/*import (
	"github.com/golang/mock/gomock"
	"context"
	pb "grpcProject/models"
	"google.golang.org/grpc"
)*/



/*
// Mock of NoteClient interface
type MockNoteClient struct {
	ctrl *gomock.Controller
	recorder *_MockNoteClientRecorder
}

// Recorder for MockNoteClient (not exported)
type _MockNoteClientRecorder struct {
	mock *MockNoteClient
}

func NewMockNoteClient(ctrl *gomock.Controller) *MockNoteClient  {
	mock := &MockNoteClient{ctrl: ctrl}
	mock.recorder = &_MockNoteClientRecorder{mock}
	return mock
}

func (_m *MockNoteClient) EXPECT() *_MockNoteClientRecorder{
	return _m.recorder
}

/////////////////
func (_m *MockNoteClient) GetAllNotes(ctx context.Context, in *pb.NoteFilter, opts ...grpc.CallOption) (pb.Note_GetAllNotesClient, error) {
	_s := []interface{}{ctx, in}
	for _, _x := range opts {
		_s = append(_s, _x)
	}
	ret := _m.ctrl.Call(_m, "GetAllNotes", _s...)

	//stream, err := client.GetAllNotes(ctx, filter)
	//customer, err := stream.Recv()
	//ret0, _ := ret[0].(*pb.Note_GetAllNotesClient)
	ret1, _ := ret[1].(error)
	return nil, ret1 // todo hz kak realizovat'
}
func (_m *MockNoteClient) CreateNote(ctx context.Context, in *pb.NoteRequest, opts ...grpc.CallOption) (*pb.NoteResponse, error) {
	_s := []interface{}{ctx, in}
	for _, _x := range opts {
		_s = append(_s, _x)
	}
	ret := _m.ctrl.Call(_m, "CreateNote", _s...)
	ret0, _ := ret[0].(*pb.NoteResponse)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}
func (_m *MockNoteClient) StopServer(ctx context.Context, in *pb.StopServerRequest, opts ...grpc.CallOption) (*pb.StopServerResponse, error) {
	_s := []interface{}{ctx, in}
	for _, _x := range opts {
		_s = append(_s, _x)
	}
	ret := _m.ctrl.Call(_m, "StopServer", _s...)
	ret0, _ := ret[0].(*pb.StopServerResponse)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}
func (_m *MockNoteClient) DelNoteByNumber(ctx context.Context, in *pb.NoteByNumRequest, opts ...grpc.CallOption) (*pb.NoteResponse, error) {
	_s := []interface{}{ctx, in}
	for _, _x := range opts {
		_s = append(_s, _x)
	}
	ret := _m.ctrl.Call(_m, "DelNoteByNumber", _s...)
	ret0, _ := ret[0].(*pb.NoteResponse)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}
func (_m *MockNoteClient) SearchNoteByNum(ctx context.Context, in *pb.NoteByNumRequest, opts ...grpc.CallOption) (*pb.NoteResponse, error) {
	_s := []interface{}{ctx, in}
	for _, _x := range opts {
		_s = append(_s, _x)
	}
	ret := _m.ctrl.Call(_m, "SearchNoteByNum", _s...)
	ret0, _ := ret[0].(*pb.NoteResponse)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}
func (_m *MockNoteClient) UpdateNote(ctx context.Context, in *pb.NoteRequest, opts ...grpc.CallOption) (*pb.NoteResponse, error) {
	_s := []interface{}{ctx, in}
	for _, _x := range opts {
		_s = append(_s, _x)
	}
	ret := _m.ctrl.Call(_m, "UpdateNote", _s...)
	ret0, _ := ret[0].(*pb.NoteResponse)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}


func (_mr *_MockNoteClientRecorder) GetAllNotes(arg0, arg1 interface{}, arg2 ...interface{}) *gomock.Call {
	_s := append([]interface{}{arg0, arg1}, arg2...)
	return _mr.mock.ctrl.RecordCall(_mr.mock, "GetAllNotes", _s...)
}
func (_mr *_MockNoteClientRecorder) CreateNote(arg0, arg1 interface{}, arg2 ...interface{}) *gomock.Call {
	_s := append([]interface{}{arg0, arg1}, arg2...)
	return _mr.mock.ctrl.RecordCall(_mr.mock, "CreateNote", _s...)
}
func (_mr *_MockNoteClientRecorder) StopServer(arg0, arg1 interface{}, arg2 ...interface{}) *gomock.Call {
	_s := append([]interface{}{arg0, arg1}, arg2...)
	return _mr.mock.ctrl.RecordCall(_mr.mock, "StopServer", _s...)
}
func (_mr *_MockNoteClientRecorder) DelNoteByNumber(arg0, arg1 interface{}, arg2 ...interface{}) *gomock.Call {
	_s := append([]interface{}{arg0, arg1}, arg2...)
	return _mr.mock.ctrl.RecordCall(_mr.mock, "DelNoteByNumber", _s...)
}
func (_mr *_MockNoteClientRecorder) SearchNoteByNum(arg0, arg1 interface{}, arg2 ...interface{}) *gomock.Call {
	_s := append([]interface{}{arg0, arg1}, arg2...)
	return _mr.mock.ctrl.RecordCall(_mr.mock, "SearchNoteByNum", _s...)
}
func (_mr *_MockNoteClientRecorder) UpdateNote(arg0, arg1 interface{}, arg2 ...interface{}) *gomock.Call {
	_s := append([]interface{}{arg0, arg1}, arg2...)
	return _mr.mock.ctrl.RecordCall(_mr.mock, "UpdateNote", _s...)
}*/


