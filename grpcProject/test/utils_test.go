package main

import (
	"testing"
	"grpcProject/server/utils"
	. "github.com/smartystreets/goconvey/convey"
)

//go get github.com/smartystreets/goconvey
func TestValidatePhone(t *testing.T) {
	testPhone1 := "+380946521570"
	wrongTestPhone2 := "380946521570"
	wrongTestPhone3 := "3380946521570"
	wrongTestPhone4 := "+3809465215"
	wrongTestPhone5 := "+38094521570"
	wrongTestPhone6 := "+3809465215705"

	Convey("Phone validating should work correctly", t, func() {
		So(utils.ValidatePhone(testPhone1), ShouldEqual, true)
		So(utils.ValidatePhone(wrongTestPhone2), ShouldEqual, false)
		So(utils.ValidatePhone(wrongTestPhone3), ShouldEqual, false)
		So(utils.ValidatePhone(wrongTestPhone4), ShouldEqual, false)
		So(utils.ValidatePhone(wrongTestPhone5), ShouldEqual, false)
		So(utils.ValidatePhone(wrongTestPhone6), ShouldEqual, false)


	})
}
