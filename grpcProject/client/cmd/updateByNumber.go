// Copyright © 2017 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	pb "grpcProject/models"
	pbe "grpcProject/entity"

	"github.com/spf13/cobra"
	"time"
	"google.golang.org/grpc"
	"log"
	"context"
)

// updateByNumberCmd represents the updateByNumber command
var updateByNumberCmd = &cobra.Command{
	Use:   "updateByNumber",
	Short: "A brief description of your command",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {
		// Set up a connection to the gRPC server.
		conn, err := grpc.Dial(ADDRESS, grpc.WithInsecure())
		if err != nil {
			log.Fatalf("did not connect: %v", err)
		}
		defer conn.Close()
		// Creates a new CustomerClient

		client := pb.NewNoteServiceClient(conn)
		ctx, _ := context.WithTimeout(context.Background(), DEADLINE_SECONDS*time.Second)

		updateNote := &pbe.Note{
			Phone:    args[0],
			Name:     args[1],
			LastName: args[2],
			HomeAddress: &pbe.Note_Address{
				City:        args[3],
				Streat:      args[4],
				HouseNumber: args[5],
			}}
		req := &pb.Request{Note: updateNote}
		resp, err := client.UpdateNote(ctx, req)
		if err != nil {
			log.Fatalf("Response status: %v", resp.Status)
			log.Fatalf("Could not update Note: %v", err)
		} else {
			log.Fatalf("Response status: %v", resp.Status)
			log.Printf("Note is updated: %d", resp.GetNote())
		}
	},
}

func init() {
	rootCmd.AddCommand(updateByNumberCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// updateByNumberCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// updateByNumberCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
