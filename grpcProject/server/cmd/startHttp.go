// Copyright © 2018 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"fmt"
	pb "grpcProject/models"
	"github.com/spf13/cobra"
	"os"
	"github.com/rs/cors"
	"strings"
	"path"
	"net/http"
	"log"
	"context"
	"google.golang.org/grpc"
	"github.com/grpc-ecosystem/grpc-gateway/runtime"
)

// startHttpCmd represents the startHttp command
var startHttpCmd = &cobra.Command{
	Use:   "startHttp",
	Short: "A brief description of your command",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {
		startHttp()
	},
}


func startHttp() {
	// Start the HTTP server for Rest
	log.Println("Starting HTTP server on port " + httpPort)
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	gw := runtime.NewServeMux()
	opts := []grpc.DialOption{grpc.WithInsecure()}
	err := pb.RegisterNoteServiceHandlerFromEndpoint(ctx, gw, "localhost"+grpcPort, opts)
	if err != nil {
		log.Fatal(err)
	}
	mux := http.NewServeMux()
	mux.HandleFunc("/swagger/", serveSwagger)
	curdir, _ := os.Getwd()
	fmt.Println("cur dir", curdir)
	//swagger := http.FileServer(http.Dir(filepath.Join(curdir, "3rdparty", "swagger-ui")))
	//mux.Handle("/swagger/", swagger)
	mux.Handle("/api/", gw)

	fs := http.FileServer(http.Dir("static"))
	mux.Handle("/", fs)

	//http.ListenAndServe(httpPort, mux)

	//cors
	c := cors.New(cors.Options{
		AllowedOrigins: []string{"*"},
		//AllowCredentials: true,
		AllowedMethods: []string{"GET", "HEAD", "POST", "PUT", "OPTIONS"},
		AllowedHeaders: []string{"X-Requested-With"},
	})
	handler := c.Handler(mux)
	//

	http.ListenAndServe(httpPort, handler)
}

func serveSwagger(w http.ResponseWriter, r *http.Request) {
	//swagger := http.FileServer(http.Dir("./3rdparty/swagger-ui"))
	fmt.Println("request", r.URL.Path)
	p := strings.TrimPrefix(r.URL.Path, "/swagger/")
	p = path.Join("3rdparty/swagger-ui/", p)
	fmt.Println("request map ", p)
	http.ServeFile(w, r, p)

}

func init() {
	rootCmd.AddCommand(startHttpCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// startHttpCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// startHttpCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
