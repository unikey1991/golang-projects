package cmd

import (
	"testing"

	pb "grpcProject/models"
	pbe "grpcProject/entity"
	"grpcProject/server/dao"
	"google.golang.org/grpc"
	"github.com/boltdb/bolt"
	"time"
	"net"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/smartystreets/assertions/should"
	"fmt"
	"github.com/golang/protobuf/proto"
	"errors"
	"context"
	"os"
	 se "grpcProject/server/servicesImpl"
)

var errTestOpenClient error
var conn *grpc.ClientConn
var testClient pb.NoteServiceClient
var testServ *grpc.Server = nil


func TestMain(m *testing.M) {
	intializeTestigDB()
	go startTestingServer()
	testClient, errTestOpenClient = testOpenClient()
	os.Exit(m.Run())
}

func TestServerFunctions(t *testing.T) {

	Convey("Open connection and testClient test", t, func() {

		So(errTestOpenClient, should.BeNil)
		So(testClient, ShouldNotBeNil)
	})

}

func TestUpadateNote(t *testing.T) {
	Convey("UpdateNote should work correctly", t, func() {
		ctx, _ := context.WithTimeout(context.Background(), DEADLINE_SECONDS*time.Second)
		/*Convey("Test for entity exist in DB", func() {
			req := &pb.Request{Req: &pb.Request_RequestByPhoneNumber{RequestByPhoneNumber: "+380101454143"}}
			resp, err := testClient.SearchNoteByNum(ctx, req)

			So(err, ShouldBeNil)
			So(resp.GetNote().Phone, ShouldEqual, "+380101454143")
			So(resp.GetNote().Name, ShouldEqual, "ntest")
			So(resp.GetNote().LastName, ShouldEqual, "ltest")
			So(resp.GetNote().HomeAddress.City, ShouldEqual, "ctest")
			So(resp.GetNote().HomeAddress.Streat, ShouldEqual, "stest")
			So(resp.GetNote().HomeAddress.HouseNumber, ShouldEqual, "hntest")
			So(resp.Status, ShouldEqual, pb.Response_OK)
		})
		Convey("Test for update existing entity", func() {
			note := &pbe.Note{
				Phone:    "+380101454143",
				Name:     "aaa",
				LastName: "bbb",
				HomeAddress: &pbe.Note_Address{
					City:        "ccc",
					Streat:      "ddd",
					HouseNumber: "eee",
				}}
			req := &pb.Request{Req: &pb.Request_Note{Note: note}}

			resp, err := testClient.UpdateNote(ctx, req)
			So(err, ShouldBeNil)
			So(resp.GetNote(), ShouldEqual, note)
			So(resp.Status, ShouldEqual, pb.Response_UPDATED)
		})
		Convey("Test for update entity is successful", func() {
			req := &pb.Request{Req: &pb.Request_RequestByPhoneNumber{RequestByPhoneNumber: "+380101454143"}}
			resp, err := testClient.SearchNoteByNum(ctx, req)

			So(err, ShouldBeNil)
			So(resp.GetNote().Phone, ShouldEqual, "+380101454143")
			So(resp.GetNote().Name, ShouldEqual, "aaa")
			So(resp.GetNote().LastName, ShouldEqual, "bbb")
			So(resp.GetNote().HomeAddress.City, ShouldEqual, "ccc")
			So(resp.GetNote().HomeAddress.Streat, ShouldEqual, "ddd")
			So(resp.GetNote().HomeAddress.HouseNumber, ShouldEqual, "eee")
			So(resp.Status, ShouldEqual, pb.Response_OK)
		})*/
		Convey("Test for update not existing entity", func() {
			note := &pbe.Note{
				Phone:    "+380506666666",
				Name:     "aaa",
				LastName: "bbb",
				HomeAddress: &pbe.Note_Address{
					City:        "ccc",
					Streat:      "ddd",
					HouseNumber: "eee",
				}}
			req := &pb.Request{Req: &pb.Request_Note{Note: note}}
			resp, err := testClient.UpdateNote(ctx, req)
			So(err, ShouldBeNil)
			So(resp.Status, ShouldEqual, pb.Response_BAD_REQUEST)
		})
	})
}

func TestDelNoteByNumber(t *testing.T) {
	Convey("DelNoteByNumber should work correctly", t, func() {
		ctx, _ := context.WithTimeout(context.Background(), DEADLINE_SECONDS*time.Second)

		Convey("Test for entity existing in DB", func() {
			req := &pb.Request{Req: &pb.Request_RequestByPhoneNumber{RequestByPhoneNumber: "+380101414143"}}
			resp, err := testClient.SearchNoteByNum(ctx, req)

			So(err, ShouldBeNil)
			So(resp.Status, ShouldEqual, pb.Response_OK)
			So(resp.GetNote().Phone, ShouldEqual, "+380101414143")

		})
		Convey("Test for delete entity", func() {
			req := &pb.Request{Req: &pb.Request_RequestByPhoneNumber{RequestByPhoneNumber: "+380101414143"}}
			resp, err := testClient.DelNoteByNumber(ctx, req)
			So(err, ShouldBeNil)
			So(resp.Status, ShouldEqual, pb.Response_OK)
		})
		Convey("Test for delete operation is successful", func() {
			req := &pb.Request{Req: &pb.Request_RequestByPhoneNumber{RequestByPhoneNumber: "+380101414143"}}
			resp, err := testClient.DelNoteByNumber(ctx, req)
			So(err, ShouldBeNil)
			So(resp.Status, ShouldEqual, pb.Response_BAD_REQUEST)
		})
		Convey("Test for delete not existing entity", func() {
			req := &pb.Request{Req: &pb.Request_RequestByPhoneNumber{RequestByPhoneNumber: "+380101414143"}}
			resp, err := testClient.DelNoteByNumber(ctx, req)
			So(err, ShouldBeNil)
			So(resp.Status, ShouldEqual, pb.Response_BAD_REQUEST)
		})
	})
}

func TestCreateNote(t *testing.T) {
	Convey("CreateNote should work correctly", t, func() {

		ctx, _ := context.WithTimeout(context.Background(), DEADLINE_SECONDS*time.Second)

		Convey("Test for create entity and return response", func() {
			note := &pbe.Note{Phone: "+380201416969",
				Name: "ntest1",
				LastName: "ltest1",
				HomeAddress: &pbe.Note_Address{
					City:        "ctest1",
					Streat:      "stest1",
					HouseNumber: "hntest1",
				}}
			req := &pb.Request{Req:&pb.Request_Note{Note: note}}

			resp, err := testClient.CreateNote(ctx, req)
			So(err, should.BeNil)
			So(resp.Status, ShouldEqual, pb.Response_CREATED)
		})
		Convey("Test for create entity with exist phone and return response", func() {
			note := &pbe.Note{Phone: "+380201416969",
				Name: "ntest1",
				LastName: "ltest1",
				HomeAddress: &pbe.Note_Address{
					City:        "ctest1",
					Streat:      "stest1",
					HouseNumber: "hntest1",
				}}
			req := &pb.Request{Req:&pb.Request_Note{Note: note}}
			resp, err := testClient.CreateNote(ctx, req)

			So(err, ShouldBeNil)
			So(resp.Status, ShouldEqual, pb.Response_SERVER_ERROR)

			//////////// delete created entity after testing
			req1 := &pb.Request{Req: &pb.Request_RequestByPhoneNumber{RequestByPhoneNumber: "+380201416969"}}
			testClient.DelNoteByNumber(ctx, req1)
			///////////
		})
		Convey("Test for create entity with wrong-format phone and return response", func() {
			note := &pbe.Note{Phone: "+3802014141",
				Name: "ntest1",
				LastName: "ltest1",
				HomeAddress: &pbe.Note_Address{
					City:        "ctest1",
					Streat:      "stest1",
					HouseNumber: "hntest1",
				}}
			req := &pb.Request{Req:&pb.Request_Note{Note: note}}
			resp, err := testClient.CreateNote(ctx, req)
			So(err, ShouldBeNil)
			So(resp.Status, ShouldEqual, pb.Response_BAD_REQUEST)
		})

	})

}

func TestSearchByNum(t *testing.T) {

	Convey("SearchNoteByNum should work correctly", t, func() {
		test1 := &pbe.Note{
			Phone:    "+380201414141",
			Name:     "ntest1",
			LastName: "ltest1",
			HomeAddress: &pbe.Note_Address{
				City:        "ctest1",
				Streat:      "stest1",
				HouseNumber: "hntest1",
			}}
		test2 := &pbe.Note{
			Phone:    "+380201414142",
			Name:     "ntest2",
			LastName: "ltest2",
			HomeAddress: &pbe.Note_Address{
				City:        "ctest2",
				Streat:      "stest2",
				HouseNumber: "hntest2",
			}}
		test3 := &pbe.Note{
			Phone:    "+380201414143",
			Name:     "ntest3",
			LastName: "ltest3",
			HomeAddress: &pbe.Note_Address{
				City:        "ctest3",
				Streat:      "stest3",
				HouseNumber: "hntest3",
			}}


		Convey("Test for first pre-created entity", func() {
			req := &pb.Request{Req: &pb.Request_RequestByPhoneNumber{RequestByPhoneNumber: "+380201414141"}}
			ctx, _ := context.WithTimeout(context.Background(), DEADLINE_SECONDS*time.Second)
			resp, err := testClient.SearchNoteByNum(ctx, req)
			So(err, ShouldBeNil)
			So(resp.GetNote().Phone, ShouldEqual, test1.Phone)
			So(resp.GetNote().Name, ShouldEqual, test1.Name)
			So(resp.GetNote().LastName, ShouldEqual, test1.LastName)
			So(resp.GetNote().HomeAddress.City, ShouldEqual, test1.HomeAddress.City)
			So(resp.GetNote().HomeAddress.Streat, ShouldEqual, test1.HomeAddress.Streat)
			So(resp.GetNote().HomeAddress.HouseNumber, ShouldEqual, test1.HomeAddress.HouseNumber)
			So(resp.Status, ShouldEqual, pb.Response_OK)

		})
		Convey("Test for second pre-created entity", func() {
			req := &pb.Request{Req: &pb.Request_RequestByPhoneNumber{RequestByPhoneNumber: "+380201414142"}}
			ctx, _ := context.WithTimeout(context.Background(), DEADLINE_SECONDS*time.Second)
			resp, err := testClient.SearchNoteByNum(ctx, req)
			So(err, ShouldBeNil)
			So(resp.GetNote().Phone, ShouldEqual, test2.Phone)
			So(resp.GetNote().Name, ShouldEqual, test2.Name)
			So(resp.GetNote().LastName, ShouldEqual, test2.LastName)
			So(resp.GetNote().HomeAddress.City, ShouldEqual, test2.HomeAddress.City)
			So(resp.GetNote().HomeAddress.Streat, ShouldEqual, test2.HomeAddress.Streat)
			So(resp.GetNote().HomeAddress.HouseNumber, ShouldEqual, test2.HomeAddress.HouseNumber)
			So(resp.Status, ShouldEqual, pb.Response_OK)

		})
		Convey("Test for third pre-created entity", func() {
			req := &pb.Request{Req: &pb.Request_RequestByPhoneNumber{RequestByPhoneNumber: "+380201414143"}}
			ctx, _ := context.WithTimeout(context.Background(), DEADLINE_SECONDS*time.Second)
			resp, err := testClient.SearchNoteByNum(ctx, req)
			So(err, ShouldBeNil)
			So(resp.GetNote().Phone, ShouldEqual, test3.Phone)
			So(resp.GetNote().Name, ShouldEqual, test3.Name)
			So(resp.GetNote().LastName, ShouldEqual, test3.LastName)
			So(resp.GetNote().HomeAddress.City, ShouldEqual, test3.HomeAddress.City)
			So(resp.GetNote().HomeAddress.Streat, ShouldEqual, test3.HomeAddress.Streat)
			So(resp.GetNote().HomeAddress.HouseNumber, ShouldEqual, test3.HomeAddress.HouseNumber)
			So(resp.Status, ShouldEqual, pb.Response_OK)

		})
		Convey("Test for not existing entity", func() {
			req := &pb.Request{Req: &pb.Request_RequestByPhoneNumber{RequestByPhoneNumber: "+380201414100"}}
			ctx, _ := context.WithTimeout(context.Background(), DEADLINE_SECONDS*time.Second)
			resp12, err333 := testClient.SearchNoteByNum(ctx, req)
			//So(resp12.Status, ShouldEqual, pb.Response_BAD_REQUEST)
			So(resp12.Status, ShouldEqual, pb.Response_BAD_REQUEST)
			So(err333, ShouldBeNil)
		})

	})
}

func intializeTestigDB() error {
	//os.Remove("grpcProject/test.db")

	dao.DB, errTestOpenClient = bolt.Open(dao.PATH_TO_DB, 0600, &bolt.Options{Timeout: 1 * time.Second})
	if errTestOpenClient != nil {
		return errTestOpenClient
	}
	//defer db.Close()
	err := dao.DB.Update(func(tx *bolt.Tx) error {
		bkt, err := tx.CreateBucketIfNotExists([]byte(dao.BucketName))
		if err != nil {
			return fmt.Errorf("create bucket: %s", err)
		}
		//BEGIN for test crateNote
		note1 := &pbe.Note{
			Phone:    "+380201414141",
			Name:     "ntest1",
			LastName: "ltest1",
			HomeAddress: &pbe.Note_Address{
				City:        "ctest1",
				Streat:      "stest1",
				HouseNumber: "hntest1",
			}}
		note2 := &pbe.Note{
			Phone:    "+380201414142",
			Name:     "ntest2",
			LastName: "ltest2",
			HomeAddress: &pbe.Note_Address{
				City:        "ctest2",
				Streat:      "stest2",
				HouseNumber: "hntest2",
			}}
		note3 := &pbe.Note{
			Phone:    "+380201414143",
			Name:     "ntest3",
			LastName: "ltest3",
			HomeAddress: &pbe.Note_Address{
				City:        "ctest3",
				Streat:      "stest3",
				HouseNumber: "hntest3",
			}}
		if buf, err := proto.Marshal(note1); err != nil {
			return errors.New("Marshaling error")
		} else if err := bkt.Put([]byte(note1.Phone), buf); err != nil {
			fmt.Println(err)
			return errors.New("Writing in base error")
		}
		if buf, err := proto.Marshal(note2); err != nil {
			return errors.New("Marshaling error")
		} else if err := bkt.Put([]byte(note2.Phone), buf); err != nil {
			fmt.Println(err)
			return errors.New("Writing in base error")
		}
		if buf, err := proto.Marshal(note3); err != nil {
			return errors.New("Marshaling error")
		} else if err := bkt.Put([]byte(note3.Phone), buf); err != nil {
			fmt.Println(err)
			return errors.New("Writing in base error")
		}
		//END for test crateNote

		//BEGIN for test deleteNote
		note4 := &pbe.Note{
			Phone:    "+380101414143",
			Name:     "ntest4",
			LastName: "ltest4",
			HomeAddress: &pbe.Note_Address{
				City:        "ctest4",
				Streat:      "stest4",
				HouseNumber: "hntest4",
			}}
		if buf, err := proto.Marshal(note4); err != nil {
			return errors.New("Marshaling error")
		} else if err := bkt.Put([]byte(note4.Phone), buf); err != nil {
			fmt.Println(err)
			return errors.New("Writing in base error")
		}
		//END for test deleteNote
		//BEGIN for test UpdateNote
		note5 := &pbe.Note{
			Phone:    "+380101454143",
			Name:     "ntest",
			LastName: "ltest",
			HomeAddress: &pbe.Note_Address{
				City:        "ctest",
				Streat:      "stest",
				HouseNumber: "hntest",
			}}
		if buf, err := proto.Marshal(note5); err != nil {
			return errors.New("Marshaling error")
		} else if err := bkt.Put([]byte(note5.Phone), buf); err != nil {
			fmt.Println(err)
			return errors.New("Writing in base error")
		}
		//END for test UpdateNote
		return nil
	})
	if err != nil {
		fmt.Println("begin error", err)
		return err
	}
	return nil
}

func testOpenClient() (pb.NoteServiceClient, error) {
	conn, errTestOpenClient = grpc.Dial(ADDRESS, grpc.WithInsecure())
	if errTestOpenClient != nil {
		return nil, errTestOpenClient
	}
	client := pb.NewNoteServiceClient(conn)
	return client, nil
}

func startTestingServer() (error) {
	var err error

	lis, err := net.Listen("tcp", port)
	if err != nil {
		return err
	}

	// Creates a new gRPC server
	testServ = grpc.NewServer()
	pb.RegisterNoteServiceServer(testServ, &se.Server{})
	err = testServ.Serve(lis)
	if err != nil {
		return err
	}
	return nil

}
