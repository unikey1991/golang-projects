// Copyright © 2017 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	pb "grpcProject/models"
	se "grpcProject/server/servicesImpl"
	"grpcProject/server/dao"
	"github.com/spf13/cobra"
	"google.golang.org/grpc"
	"log"
	"net"

)

const (
	grpcPort = ":8082"
	httpPort = ":8081"
)


//go:generate protoc -I. -I%GOPATH%\src -I%GOPATH%\src\github.com\grpc-ecosystem\grpc-gateway\ -I%GOPATH%\src\github.com\grpc-ecosystem\grpc-gateway\third_party\googleapis --go_out=plugins=grpc:. --grpc-gateway_out=logtostderr=true:. --swagger_out=logtostderr=true:. models/phoneBook.proto
//copy %GOPATH%\src\grpcProject\models\phoneBook.swagger.json %GOPATH%\src\grpcProject\3rdparty\swagger-ui\phoneBook.swagger.json

// startServerCmd represents the startServer command
var startServerCmd *cobra.Command = &cobra.Command{
	Use:   "startServer",
	Short: "A brief description of your command",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {

		log.Println("Starting application")

		dao.OpenConnectionToDB()

		startGrpc()



		//serve()

		//startServer()

	},
}


func startGrpc() {
	// start listening for grpc
	listen, err := net.Listen("tcp", grpcPort)
	if err != nil {
		log.Fatal(err)
	}
	// Instanciate new gRPC server
	server := grpc.NewServer()
	//register service
	pb.RegisterNoteServiceServer(server, new(se.Server))

	log.Println("Starting grpc server on port " + grpcPort)

	// Start the gRPC server in goroutine
	//go server.Serve(listen)
	server.Serve(listen)

}
func init() {
	rootCmd.AddCommand(startServerCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// startServerCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// startServerCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
