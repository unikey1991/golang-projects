package cmd

import (
	pb "grpcProject/models"
	//pbe "grpcProject/entity"
	"grpcProject/server/dao"

	"grpcProject/server/utils"
	context2 "golang.org/x/net/context"
	//"github.com/golang/protobuf/proto"
	"log"
	"fmt"
)

type Server struct{}


func NewServer() *Server {
	return new(Server)
}

func (s *Server) GetAllNotes(ctx context2.Context, request *pb.Request) (*pb.ListResponse, error) {

	/*bkt := dao.GetAll()
	bkt.ForEach(func(k, v []byte) error {
		note := &pbe.Note{}
		if err := proto.Unmarshal(v, note); err != nil {
			return err
		}*/

	//response := &pb.Response{Note:note, Status:pb.Response_OK}

	noteList, err := dao.GetAll()
	if err != nil {
		log.Fatalf("dao.GetAll() erro: %v", err)
	}
	response := &pb.ListResponse{Note:noteList}

	/*for _, note := range noteList {
		response := &pb.Response{Note: note, Status: pb.Response_OK}
		if err := stream.Send(response); err != nil {
			return err
		}
	}*/

	return response, nil

	/*})

	return nil*/
}

func (s *Server) CreateNote(ctx context2.Context, request *pb.Request) (*pb.Response, error) {
	fmt.Printf("CreateNote: %d \n", request.Note)
	if !utils.ValidatePhone(request.GetNote().Phone) {
		return &pb.Response{Status: pb.Response_BAD_REQUEST}, nil
		//return nil, fmt.Errorf("Wrong phone format: %s", request.GetNote().Phone)
	}
	err := dao.NewNote(request.GetNote())
	if err != nil {
		return &pb.Response{Status: pb.Response_SERVER_ERROR}, nil
	} else {
		return &pb.Response{Status: pb.Response_CREATED}, nil
	}

}

func (s *Server) DelNoteByNumber(ctx context2.Context, request *pb.RequestByPhoneNumber) (*pb.Response, error) {
	fmt.Print(request.PhoneNumber)
	err := dao.DeleteNote(request.PhoneNumber)
	if err != nil {
		return &pb.Response{Status: pb.Response_BAD_REQUEST}, nil
	} else {
		return &pb.Response{Status: pb.Response_OK}, nil
	}
}

func (s *Server) SearchNoteByNum(ctx context2.Context, request *pb.RequestByPhoneNumber) (*pb.Response, error) {
	note, err := dao.GetNoteByNumber(request.PhoneNumber)
	if err != nil {
		if err != nil {

		}
		return &pb.Response{Status: pb.Response_BAD_REQUEST}, nil
	} else {
		return &pb.Response{Note: note, Status: pb.Response_OK}, nil
	}
}


func (s *Server) UpdateNote(ctx context2.Context, request *pb.Request) (*pb.Response, error) {
	//err := dao.UpdateNote(request.GetNote())
	err := dao.UpdateNote(request.GetNote())

	if err != nil {
		return &pb.Response{Status: pb.Response_BAD_REQUEST}, nil
	} else {
		//return &pb.Response{Note: request.GetNote(), Status: pb.Response_OK}, nil
		return &pb.Response{Note: nil, Status: pb.Response_OK}, nil
	}

}

/*
enum CompanyType {
Private = 17;
Public = 18;
NonProfit = 19;
};

message Company {
required string Name = 1;
repeated Employee Employees = 2;
required CompanyType Type = 3;
optional group Address = 4 {
required string Country = 5;
required string City = 6;
optional string Street = 7;
}
}*/
