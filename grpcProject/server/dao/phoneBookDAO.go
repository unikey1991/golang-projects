package dao

import (
	"github.com/boltdb/bolt"
	"time"
	pbe "grpcProject/entity"
	"fmt"
	"github.com/golang/protobuf/proto"
	"errors"
)

var DB *bolt.DB = nil

const PATH_TO_DB = "C:/Users/aleks/Desktop/GolandProjects/SRC/grpcProject/my.db"
const BucketName = "NOTES"

func OpenConnectionToDB() {
	//bolt start
	DB, _ = bolt.Open(PATH_TO_DB, 0600, &bolt.Options{Timeout: 1 * time.Second})
}

func CloseConnectionToDB() {
	DB.Close()
}

func GetAll() ([]*pbe.Note, error) {
	var noteList []*pbe.Note

	err := DB.Update(func(tx *bolt.Tx) error {
		bkt := tx.Bucket([]byte(BucketName))
		if bkt == nil {
			return errors.New("Bucket not exist, create first")
		}
		bkt.ForEach(func(k, v []byte) error {
			note := &pbe.Note{}
			if err := proto.Unmarshal(v, note); err != nil {
				return err
			}
			noteList = append(noteList, note)
			return nil
		})

		return nil
	})
	if err != nil {
		return nil, err
	}
	return noteList, nil
}

func UpdateNote(note *pbe.Note) (err error) {
	err = DB.Update(func(tx *bolt.Tx) error {
		bkt := tx.Bucket([]byte(BucketName))
		if bkt == nil {
			return errors.New("Bucket not exist, create first")
		}

		if row := bkt.Get([]byte(note.Phone)); row != nil {
			if buf, err := proto.Marshal(note); err != nil {
				return err
			} else if err := bkt.Put([]byte(note.Phone), buf); err != nil {
				return err
			}
		} else if row == nil {
			return errors.New("Номер " + note.Phone + " не найден в базе")
		}
		return err
	})
	if err != nil {
		return err
	}
	return nil

}

func GetNoteByNumber(number string) (*pbe.Note, error) {
	note := &pbe.Note{}
	err := DB.Update(func(tx *bolt.Tx) error {
		bkt := tx.Bucket([]byte(BucketName))
		if bkt == nil {
			return errors.New("Bucket not exist, create first")
		}

		buf := bkt.Get([]byte(number))
		if buf != nil {
			if err := proto.Unmarshal(buf, note); err != nil {
				return err
			}
		} else {
			return errors.New("Note not found")
		}
		return nil
	})
	if err != nil {
		return nil, err
	}
	return note, err

}

func DeleteNote(phoneNumber string) (error) {
	err := DB.Update(func(tx *bolt.Tx) error {
		bkt := tx.Bucket([]byte(BucketName))
		if bkt == nil {
			return errors.New("Bucket not exist, create first")
		}

		if row := bkt.Get([]byte(phoneNumber)); row == nil {
			return errors.New("Запись по указанному номеру не найдена")

		} else if err := bkt.Delete([]byte(phoneNumber)); err != nil {
			return errors.New("Запись найдена, но при удалении ошибка")
		}

		return nil
	})
	if err != nil {
		return err
	}
	return nil

}

func NewNote(note *pbe.Note) (er error) {
	er = DB.Update(func(tx *bolt.Tx) error {
		bkt, err := tx.CreateBucketIfNotExists([]byte(BucketName))
		if err != nil {
			return fmt.Errorf("create bucket: %s", err)
		}
		if row := bkt.Get([]byte(note.Phone)); row != nil {
			fmt.Println("trest")
			return errors.New("Запись с таким номером уже существует")
		}
		if buf, err := proto.Marshal(note); err != nil {
			return errors.New("Marshaling error")
		} else if err := bkt.Put([]byte(note.Phone), buf); err != nil {
			fmt.Println(err)
			return errors.New("Writing in base error")
		}
		return nil //You commit the transaction by returning nil at the end
	})
	if er != nil {
		return er
	}
	return nil

}
