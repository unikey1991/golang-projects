Installing:
===
###### 1. Скачать данный проект <https://gitlab.com/unikey1991/golang-projects/repository/master/archive.zip> 
 и из архива скопировать grpcProject в папку проектов Go

###### 2. Установить зависимоти Cobra, protobuf, boltDB, grpc и в настройках проекта (при необходимости) установить корректный goPath:

    go get -u github.com/spf13/cobra/cobra
    go get github.com/golang/protobuf/protoc-gen-go
    go get github.com/boltdb/bolt/...
    go get -u google.golang.org/grpc

###### 3. Установка компилятора protobuf (при его отсутствии):
    
* перейдите по ссылке <https://github.com/google/protobuf/releases>
* скачайте zip архив protoc-3.5.0-win32.zip
* распакуйте и положите эту папку в $GOPATH/bin

* консоле перейдите в $GOPATH/bin/protoc-3.5.0-win32/bin
* из терминала запустите команду: `protoc -I. -I%GOPATH%\src -I%GOPATH%\src\github.com\grpc-ecosystem\grpc-gateway\ -I%GOPATH%\src\github.com\grpc-ecosystem\grpc-gateway\third_party\googleapis --go_out=plugins=grpc:. --grpc-gateway_out=logtostderr=true:. --swagger_out=logtostderr=true:. models/phoneBook.proto`
* скопируйте phoneBook.swagger.json в пакет 3rdparty/swagger-ui (из терминала запустите команду) : `copy %GOPATH%\src\grpcProject\models\phoneBook.swagger.json %GOPATH%\src\grpcProject\3rdparty\swagger-ui\phoneBook.swagger.json`
* после выполнения у вас в директории $GOPATH\src\protobufProject\model сгенерится структура phoneBook.pb.go, phoneBook.pb.gw.go и phoneBook.swagger.json

Runing:
===

###### Для запуска GRPC сервера - открыть терминал и ввести `go run server/main.go startServer`
###### Для запуска HTTP - открыть новую вкладку с терминалом и в терминале ввести `go run server/main.go startHttp`
###### Далее открыть браузер и перейти на <http://localhost:8081/>
###### Страница с swaggerUI -  <http://localhost:8081/swagger/>



Ссылка на документацию по boltDB
===
<https://github.com/boltdb/bolt#installing>

Ссылка на документацию по grpc
===
<https://grpc.io/docs/quickstart/go.html>