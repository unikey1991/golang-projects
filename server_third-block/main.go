package main

import (
	"net"
	"log"
	"google.golang.org/grpc"
	"github.com/boltdb/bolt"
	"time"
	"fmt"
	"github.com/golang/protobuf/proto"
	model "server_third-block/models"
	"io"
	"errors"
)

const (
	pathToDb   = "D:\\my.db"
	BucketName = "NOTES"
	grpcPort   = ":8082"
)

var hasNext = true
var start time.Time
var batchStart = time.Now()
var i int64 = 0
var DB *bolt.DB

type Server struct{}

func main() {
	DB, _ = bolt.Open(pathToDb, 0600, &bolt.Options{Timeout: 1 * time.Second})
	//DB.MaxBatchSize = 10000

	start = time.Now()
	startGrpc()

}

func (Server) LoadFromFileToBoltDb(stream model.NoteService_LoadFromFileToBoltDbServer) error {
	fmt.Println("test LoadFromFileToBoltDb")

	for hasNext {
		err := DB.Batch(func(tx *bolt.Tx) error {
			bkt, err := tx.CreateBucketIfNotExists([]byte(BucketName))
			if err != nil {
				return fmt.Errorf("create bucket: %s", err)
			}

			return writeBatchToBolt(stream, bkt)
		})
		if err != nil {
			fmt.Println("code 0003")
		}
	}
	//////////
	var count int64 = 0
	DB.View(func(tx *bolt.Tx) error {
		// Assume bucket exists and has keys
		b := tx.Bucket([]byte(BucketName))

		c := b.Cursor()

		for k, _ := c.First(); k != nil; k, _ = c.Next() {
			//fmt.Printf("key=%s, value=%s\n", k, v)
			count++
		}

		return nil
	})

	return stream.SendAndClose(&model.Response{
		Status:            model.Response_OK,
		NotesRecived:      i,
		NotesInBaseWrited: count})
}

func writeBatchToBolt(stream model.NoteService_LoadFromFileToBoltDbServer, bkt *bolt.Bucket) error {
	batchStart = time.Now()
	for hasNext {
		newNote, err := stream.Recv()
		if err == io.EOF {
			t := time.Now()
			elapsed := t.Sub(start)
			fmt.Println(elapsed)
			fmt.Printf("Всего записано: %d. Всего затрачено времени %s \n\r", i, elapsed)
			hasNext = false
			break
		}
		if newNote == nil {
			continue
		}
		if buf, err := proto.Marshal(newNote); err != nil {
			fmt.Println(newNote)
			fmt.Println("code 0001")
			//fmt.Println(err)
			return errors.New("Marshaling error")
		} else if err := bkt.Put([]byte(newNote.Phone), buf); err != nil {
			if err := bkt.Put([]byte(newNote.Phone), buf); err != nil {
				fmt.Println("code 0002")
				return errors.New("Writing in base error")
			}
		}
		i++
		if i%1000000 == 0 {
			if i%1000000 == 0 {
				t := time.Now()
				elapsed := t.Sub(batchStart)
				fmt.Printf("Миллионов записано: %d. Последний миллион записан за %s \n\r", i/1000000, elapsed)
				batchStart = time.Now()
			}
			break
		}

	}
	return nil
}

func startGrpc() {
	// start listening for grpc
	listen, err := net.Listen("tcp", grpcPort)
	if err != nil {
		log.Fatal(err)
	}
	// Instanciate new gRPC server
	server := grpc.NewServer()
	//register service
	model.RegisterNoteServiceServer(server, new(Server))

	log.Println("Starting grpc server on port " + grpcPort)

	// Start the gRPC server in goroutine
	//go server.Serve(listen)
	server.Serve(listen)

}
