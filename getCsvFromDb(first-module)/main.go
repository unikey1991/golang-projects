package main

import (
	"github.com/jmoiron/sqlx"
	"github.com/joho/sqltocsv"
	"strconv"
	"time"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/rmulley/go-fast-sql"
	"net/url"
	"log"
)

const (
	dbConnString = "root:qwerty@tcp(localhost:3306)/testgrpc"
	csvName      = "D:\\test.csv"
	BucketName   = "NOTES"
	tableName    = "phonebook1"
)


func main() {

	start := time.Now()



	getCsvFromMysql() // выгрузка из мускула в csv



	t := time.Now()
	elapsed := t.Sub(start)
	fmt.Println(elapsed)


	//generateFakeNotesInDB() //для генеражжи записей
}


func getCsvFromMysql() {
	conn, err := sqlx.Connect("mysql", dbConnString)
	if err != nil {
		panic(err)
	}
	rows, _ := conn.Query("SELECT * FROM " + tableName + " ORDER by phoneNumber")
	sqltocsv.WriteFile(csvName, rows)
}

func generateFakeNotesInDB() {

	var dbh *fastsql.DB
	var err error

	if dbh, err = fastsql.Open("mysql", "root:qwerty@tcp(localhost:3306)/testgrpc?"+url.QueryEscape("charset=utf8mb4,utf8&loc=America/New_York"), 10000); err != nil {
		log.Fatalln(err)
	}
	defer dbh.Close()

	tel := 380099999999
	for i := 0; i < 50000000; i++ {
		num := "+" + strconv.Itoa(tel+i)

		if err = dbh.BatchInsert("INSERT INTO " + tableName + " (phoneNumber, name, lastName, city, street, houseNumber) VALUES(?, ?, ?, ?, ?, ?);", num, "name", "lastName", "city", "street", "house number"); err != nil {
			log.Fatalln(err)
		}

	}

}

